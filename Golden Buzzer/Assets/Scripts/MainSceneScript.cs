﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainSceneScript : MonoBehaviour {

    int counter = 0;
    public AudioSource SoundClip;
    public UnityEngine.Video.VideoPlayer VideoPlayer;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void BuzzerPressed()
    {
        counter++;
        if (counter % 2 == 0)
        {
            SoundClip.Stop();
            VideoPlayer.Stop();
        } else
        {
            SoundClip.Play();
            VideoPlayer.Play();
        }

        int number = Random.Range(1, 100);
        if (number < 21)
        {
            SoundClip.Stop();
            VideoPlayer.Stop();
            SceneManager.LoadScene("NativeAdScene");
        }
    }
}
